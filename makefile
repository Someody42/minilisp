# Generic GNUMakefile

# Just a snippet to stop executing under other make(1) commands
# that won't understand these lines
ifneq (,)
This makefile requires GNU Make.
endif

CORE_CPP_FILES := $(shell find core/ -type f -name "*.cpp")
CORE_OBJ_FILES := $(patsubst %.cpp, %.o, $(CORE_CPP_FILES))
INCLUDE_PATHS := -Icore/
CC = clang++
CFLAGS = -std=c++17 -g -Wall -pedantic
LDFLAGS =
LDLIBS = -lm
SFML_LDFLAGS := -lsfml-graphics -lsfml-window -lsfml-system

all: minilisp

minilisp: main.cpp .depend $(CORE_OBJ_FILES)
	$(CC) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(CFLAGS) $(CORE_OBJ_FILES) main.cpp $(LDFLAGS) -o minilisp $(LDLIBS)

depend: .depend

.depend: cmd = g++ $(INCLUDE_PATHS) -MM -MF depend $(var); cat depend >> .depend;
.depend:
	@echo "Generating dependencies..."
	@$(foreach var, $(CORE_CPP_FILES), $(cmd))
	@rm -f depend

-include .depend

# These are the pattern matching rules. In addition to the automatic
# variables used here, the variable $* that matches whatever % stands for
# can be useful in special cases.
%.o: %.cpp
	$(CC) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(CFLAGS) -c $< -o $@

%: %.cpp
	$(CC) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(CFLAGS) -o $@ $<

clean:
	rm -f .depend $(CORE_OBJ_FILES)

.PHONY: clean depend
