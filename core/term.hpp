#ifndef TERM_HPP
#define TERM_HPP

#include <cstdlib>
#include <string>

#include "type.hpp"

enum BuiltinOp {ADD, SUB, NEG, MUL, DIV, MOD, EQ, NEQ, SUP, SUPEQ, INF, INFEQ, NOT, AND, OR, IF, PRINT, CONS, TAIL, HEAD, ISNIL, RANDINT}; //TBC

struct Term;
struct TermList;
struct Atom;
struct Parameter;
struct Operation;
struct Function;
struct Arity;

struct Arity{
  Type* types;
  size_t size;
};

struct Variable{
  size_t id;
};

struct Operation{
  BuiltinOp op;
  Term* parameters;
};

struct Call{
  Function* function;
  Term* parameters;
};

struct Term{
  enum {VALUE, VARIABLE, OPERATION, CALL} tag;
  union {
    Val value;
    Variable variable;
    Operation operation;
    Call call;
  };
  Type type; //Type of the term at evaluation
                     //(calculated at construction from subterm types)
};

struct Function{//Une fonction = terme paramétré
  Term* p_term;
  Arity arity;
  Type rettype;
};

Arity arity(BuiltinOp op);

void initValueTerm(Term* p_term, Val val);
void initVariableTerm(Term* p_term, size_t id, Type type);
void initOperationTerm(Term* p_term, BuiltinOp op, Term* parameters);
void initCallTerm(Term* p_term, Function* function, Term* parameters);

void destroyTerm(Term* ptr);

#endif
