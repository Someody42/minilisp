#include "parser.hpp"

void parseAtomic(Term* p_term, std::string atom, VariablePool* varpool) {
  if(atom == "true") initValueTerm(p_term, boolVal(true));
  else if(atom == "false") initValueTerm(p_term, boolVal(false));
  else if(atom == "nil" || atom == "{}" || atom == "[]") initValueTerm(p_term, nilVal());
  else {
    try{
      initValueTerm(p_term, intVal(std::stol(atom)));
    }catch(...){
      if(varpool != nullptr) {
        VariableListNode* it = varpool->p_begin;
        size_t i = 0;
        while(it != nullptr) {
          if(atom == it->name) {
            initVariableTerm(p_term, i, it->type);
            return;
          }
          it = it->p_next;
          ++i;
        }
      }
      throw MinilispError{MinilispError::UNKNOWN_SYMBOL, "Symbole atomique "+atom+" inconnu"};
    }
  }
}

void parseTerm(Term* p_term, std::istream& stream, FunctionPool* functionPool, VariablePool* varpool) {
  static int tab = -1;
  if(!stream || stream.eof()) {std::cerr<<"Expected term"<<std::endl; throw MinilispError{MinilispError::EXPECTED_TERM,"Terme attendu : fin du fichier atteinte"};}
  tab++;
  std::string first_block;
  bool is_atomic = true;
  std::stringstream first_block_stream;
  if(stream.peek() == '(') {
    is_atomic = false;
    stream.get();
  }
  char c = stream.peek();
  while(stream && !stream.eof() && !std::isspace(c) && c != ')') {
    first_block_stream << c;
    stream.get();
    c = stream.peek();
  }
  first_block = first_block_stream.str();
  /*for (int i = 0; i < tab; i++) {
    std::cout<<" ";
  }
  std::cout<<first_block<<std::endl;*/
  if(is_atomic) {
    parseAtomic(p_term,first_block,varpool);
  }else{
    BuiltinOp op;
    Function* function = nullptr;
    bool is_call = false;
    if(first_block == "+" || first_block == "add") op = ADD;
    else if(first_block == "-" || first_block == "sub") op = SUB;
    else if(first_block == "~" || first_block == "neg") op = NEG;
    else if(first_block == "*" || first_block == "mul") op = MUL;
    else if(first_block == "/" || first_block == "div") op = DIV;
    else if(first_block == "%" || first_block == "mod") op = MOD;
    else if(first_block == "=" || first_block == "eq") op = EQ;
    else if(first_block == "!=" || first_block == "neq") op = NEQ;
    else if(first_block == ">") op = SUP;
    else if(first_block == ">=") op = SUPEQ;
    else if(first_block == "<") op = INF;
    else if(first_block == "<=") op = INFEQ;
    else if(first_block == "!" || first_block == "not") op = NOT;
    else if(first_block == "&&" || first_block == "and") op = AND;
    else if(first_block == "||" || first_block == "or") op = OR;
    else if(first_block == "::" || first_block == "cons") op = CONS;
    else if(first_block == "head") op = HEAD;
    else if(first_block == "tail") op = TAIL;
    else if(first_block == "isnil") op = ISNIL;
    else if(first_block == "randint") op = RANDINT;
    else if(first_block == "?"  || first_block == "if") op = IF;
    else if(first_block == "print") op = PRINT;
    else {
      is_call = true;
      function = searchFunctionInPool(functionPool, first_block);
      if (function == nullptr) throw MinilispError{MinilispError::UNKNOWN_SYMBOL,"Symbole non-atomique "+first_block+" inconnu"};
    }

    int arity_size = is_call ? function->arity.size : arity(op).size;
    Term* params;
    try{
      params = new Term[arity_size];
    }catch(...){
      //std::cerr<<"Allocation Failed"<<std::endl;
      throw MinilispError{MinilispError::TERM_ALLOCATION_FAILED,"Erreur d'allocation. Libérez de la mémoire."};
    }

    stream >> std::ws;
    int i = 0;
    try{
      while(stream && !stream.eof() && stream.peek() != ')'){
        if (i>=arity_size) {
          throw MinilispError{MinilispError::ARITY_ERROR,"Erreur d'arité : le symbole "+first_block+" attend "+std::to_string(arity_size)+" paramètres"};
        }
        parseTerm(params+i,stream, functionPool, varpool);
        stream >> std::ws;
        ++i;
      }
      stream.get(); //On extrait la parenthèse
      if(i < arity_size) {
        //std::cerr<<"Arity error"<<std::endl;
        throw MinilispError{MinilispError::ARITY_ERROR, "Erreur d'arité : le symbole "+first_block+" attend "+std::to_string(arity_size)+" paramètres"};
      }
      try{
        //Il faut que les sous-termes soient formés avant cette ligne :
        if (is_call) initCallTerm(p_term, function, params);
        else initOperationTerm(p_term, op, params);
      }catch(MinilispError& e){
        e.msg = std::string() + "Erreur de type dans les paramètres de " + (is_call ? "la fonction " : "l'opérateur ") + first_block + " : " + e.msg;
        throw e;
      }
    }catch(MinilispError& e){
      for (int j = 0; j < i; j++) {
        destroyTerm(params+j); //On détruit ceux qu'on avait bien construits
      }
      delete params;
      throw e;
    }
  }
  tab--;
}

void initEmptyVariablePool(VariablePool* pool) {
  pool->p_begin = nullptr;
  pool->p_last_pointer = &(pool->p_begin);
}

void addVariableToPool(VariablePool* pool, std::string name, Type type) {
  *pool->p_last_pointer = new VariableListNode{name,type,nullptr};
  pool->p_last_pointer = &((*pool->p_last_pointer)->p_next);
}

void clearVariablePool(VariablePool* pool) {
  VariableListNode* tmp = pool->p_begin;
  while(pool->p_begin != nullptr) {
    pool->p_begin = pool->p_begin->p_next;
    delete tmp;
    tmp = pool->p_begin;
  }
  pool->p_last_pointer = &(pool->p_begin);
}

void loadNewFunction(std::istream& stream, FunctionPool* functionPool) {
  Function* p_function;
  try{
    p_function = new Function;
  } catch (...) {
    throw MinilispError{MinilispError::FUNCTION_ALLOCATION_FAILED,"Erreur d'allocation"};
  }
  std::string name;
  std::string header;
  std::string srettype;
  Type rettype;
  Type type;
  std::getline(stream,header,':');
  stream >> std::ws;
  std::istringstream header_stream(header);
  header_stream >> srettype;
  if (srettype == "int") rettype = INT;
  else if (srettype == "bool") rettype = BOOL;
  else if (srettype == "list") rettype = LIST;
  else throw MinilispError{MinilispError::INVALID_HEADER,"En-tête invalide : type "+srettype+" inconnu"};
  VariablePool varpool;
  initEmptyVariablePool(&varpool);
  header_stream >> name;
  header_stream >> std::ws;
  size_t paramnum = 0;
  while(header_stream.get() == '[') {
    ++paramnum;
    std::string param;
    std::getline(header_stream,param,']');
    std::string stype;
    header_stream.get();
    header_stream >> std::ws;
    std::istringstream param_stream(param);
    param_stream >> param >> stype;
    if (stype == "int") type = INT;
    else if (stype == "bool") type = BOOL;
    else if (stype == "list") type = LIST;
    else {
      delete p_function;
      clearVariablePool(&varpool);
      throw MinilispError{MinilispError::INVALID_HEADER,"En-tête invalide : type "+stype+" inconnu"};
    }
    addVariableToPool(&varpool,param,type);
  }
  addFunctionToPool(functionPool, name, p_function);
  try{
    *p_function = {new Term{},{new Type[paramnum], paramnum},rettype};
    //TODO : separate both allocations
  } catch(...) {
    clearVariablePool(&varpool);
    throw MinilispError{MinilispError::TERM_ALLOCATION_FAILED,"Erreur d'allocation"};
  }
  VariableListNode* it = varpool.p_begin;
  size_t i = 0;
  while(it != nullptr) {
    p_function->arity.types[i] = it->type;
    it = it->p_next;
    i++;
  }
  try{
    parseTerm(p_function->p_term,stream,functionPool,&varpool);
  }catch(MinilispError& e){
    destroyTerm(p_function->p_term);
    delete p_function->p_term;
    delete[] p_function->arity.types;
    delete p_function;
    clearVariablePool(&varpool);
    throw e;
  }
  clearVariablePool(&varpool);
}

void initEmptyFunctionPool(FunctionPool* pool) {
  pool->p_root = nullptr;
}

void addFunctionToPool(FunctionPool* pool, std::string name, Function* function) {
  FunctionTreeNode** ptr_to_update = &(pool->p_root);
  FunctionTreeNode* it = pool->p_root;
  while(it != nullptr) {
    if(it->name == name) {destroyTerm(it->p_function->p_term); delete it->p_function; it->p_function = function; return; }
    if(it->name > name) {ptr_to_update = &(it->p_left); it = it->p_left;}
    else {ptr_to_update = &(it->p_right); it = it->p_right;}
  }
  *ptr_to_update = new FunctionTreeNode{name, function, nullptr, nullptr};
}

void listFunctions(FunctionTreeNode* p_node, std::ostream& out) {
  if(p_node == nullptr) return;
  listFunctions(p_node->p_left, out);
  printType(p_node->p_function->rettype,out);
  out<<" "<<p_node->name;
  for (size_t i = 0; i < p_node->p_function->arity.size; i++) {
    out<<" [";
    printType(p_node->p_function->arity.types[i],out);
    out<<"]";
  }
  out<<std::endl;
  listFunctions(p_node->p_right, out);
}

void listFunctions(FunctionPool* pool, std::ostream& out) {
  listFunctions(pool->p_root, out);
}

Function* searchFunctionInPool(FunctionPool* pool, std::string name) {
  FunctionTreeNode* it = pool->p_root;
  while(it != nullptr) {
    if(it->name == name) return it->p_function;
    if(it->name > name) it = it->p_left;
    else it = it->p_right;
  }
  return nullptr;
}

void clearFunctionPool(FunctionTreeNode* p_node) {
  if(p_node == nullptr) return;
  clearFunctionPool(p_node->p_left);
  clearFunctionPool(p_node->p_right);
  delete p_node;
}

void clearFunctionPool(FunctionPool* pool) {
  clearFunctionPool(pool->p_root);
}
