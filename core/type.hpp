#ifndef TYPE_HPP
#define TYPE_HPP

#include <iostream>

struct MinilispError {
  enum Type {INVALID_HEADER, EXPECTED_TERM, ARITY_ERROR, TYPE_ERROR, UNKNOWN_SYMBOL, TERM_ALLOCATION_FAILED, FUNCTION_ALLOCATION_FAILED} type;
  std::string msg;
};

enum Type {INT, BOOL, LIST};

using Int = long;
using Bool = bool;

struct Node{
  Int val;
  Node* p_next;
  long int refcount;
};

struct List{
  Node* p_begin;
};

struct Val{
  Type type;
  union {
    Int integer;
    Bool boolean;
    List list;
  };
};

Val intVal(Int val);
Val boolVal(Bool val);
Val nilVal();

void addRef(Node* ptr);
void removeRef(Node* ptr);
void destroyList(List list);

void printType(Type type, std::ostream& out);
void printVal(Val val, std::ostream& out, bool destroyVal = false);

long int memchecker(int modifier = 0);

#endif
