#include "type.hpp"

Val intVal(Int val) {
  Val ret;
  ret.type = INT;
  ret.integer = val;
  return ret;
}

Val boolVal(Bool val) {
  Val ret;
  ret.type = BOOL;
  ret.boolean = val;
  return ret;
}

Val nilVal() {
  Val ret;
  ret.type = LIST;
  ret.list = {nullptr};
  return ret;
}

void printType(Type type, std::ostream& out) {
  switch (type) {
    case INT:
      out<<"int";
      break;
    case BOOL:
      out<<"bool";
      break;
    case LIST:
      out<<"list";
      break;
    default:
      out<<"<unknown type>";
      break;
  }
}

void addRef(Node* ptr) {
  if(ptr != nullptr) ++(ptr->refcount);
}

void removeRef(Node* ptr) {
  Node* tmp;
  while(ptr!=nullptr) {
    tmp = ptr->p_next;
    --(ptr->refcount);
    if(ptr->refcount <= 0) {
      delete ptr;
      memchecker(-1);
      ptr = tmp;
      continue;
    }
    break;
  }
}

void destroyMinilispList(List list) {
  Node* it = list.p_begin, *tmp;
  while(it!=nullptr) {
    tmp = it->p_next;
    delete it;
    memchecker(-1);
    it = tmp;
  }
}

void printVal(Val val, std::ostream& out, bool destroyVal) {
  Node* it;
  switch (val.type) {
    case BOOL:
      out<<(val.boolean ? "true" : "false")<<std::endl;
      break;
    case INT:
      out<<val.integer<<std::endl;
      break;
    case LIST:
      out<<"{ ";
      it = val.list.p_begin;
      while(it!=nullptr) {
        out<<it->val<<" ";
        it = it->p_next;
      }
      out<<"}"<<std::endl;
      if(destroyVal) destroyMinilispList(val.list);
      break;
    default:
      break;
  }
}

long int memchecker(int modifier) {
  static long int alloc_counter = 0;
  alloc_counter += modifier;
  return alloc_counter;
}
