#include "evaluation.hpp"

Val evalTerm(Term t, Val* varpool) {
  //std::cout<<varpool<<" ";
  switch (t.tag) {
    case Term::VALUE:
      //std::cout<<"V"<<std::endl;
      return t.value;
    case Term::VARIABLE:
      //std::cout<<"P"<<std::endl;
      if(t.type == LIST) addRef(varpool[t.variable.id].list.p_begin);
      return varpool[t.variable.id];
    case Term::OPERATION:
      //std::cout<<"O"<<std::endl;
      return evalBuiltinOp(t.operation.op, t.operation.parameters, varpool);
    case Term::CALL:
      //std::cout<<"C"<<std::endl;
      return evalFunctionCall((Function*)t.call.function, t.call.parameters, varpool);
  }
  return Val{};
}

Val evalBuiltinOp(BuiltinOp op, Term* p, Val* varpool) {
  Val ret;
  Node* tmp;
  switch (op) {
    case ADD:
      ret.type = INT;
      ret.integer = evalTerm(p[0],varpool).integer + evalTerm(p[1],varpool).integer;
      break;
    case SUB:
      ret.type = INT;
      ret.integer = evalTerm(p[0],varpool).integer - evalTerm(p[1],varpool).integer;
      break;
    case NEG:
      ret.type = INT;
      ret.integer = -evalTerm(p[0],varpool).integer;
      break;
    case MUL:
      ret.type = INT;
      ret.integer = evalTerm(p[0],varpool).integer * evalTerm(p[1],varpool).integer;
      break;
    case DIV:
      ret.type = INT;
      ret.integer = evalTerm(p[0],varpool).integer / evalTerm(p[1],varpool).integer;
      break;
    case MOD:
      ret.type = INT;
      ret.integer = evalTerm(p[0],varpool).integer % evalTerm(p[1],varpool).integer;
      break;
    case EQ:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).integer == evalTerm(p[1],varpool).integer;
      break;
    case NEQ:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).integer != evalTerm(p[1],varpool).integer;
      break;
    case SUP:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).integer > evalTerm(p[1],varpool).integer;
      break;
    case SUPEQ:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).integer >= evalTerm(p[1],varpool).integer;
      break;
    case INF:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).integer < evalTerm(p[1],varpool).integer;
      break;
    case INFEQ:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).integer <= evalTerm(p[1],varpool).integer;
      break;
    case NOT:
      ret.type = BOOL;
      ret.boolean = !evalTerm(p[0],varpool).boolean;
      break;
    case AND:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).boolean && evalTerm(p[1],varpool).boolean;
      break;
    case OR:
      ret.type = BOOL;
      ret.boolean = evalTerm(p[0],varpool).boolean || evalTerm(p[1],varpool).boolean;
      break;
    case CONS:
      ret.type = LIST;
      ret.list = {new Node{evalTerm(p[0],varpool).integer, evalTerm(p[1],varpool).list.p_begin, 1}};
      memchecker(1);
      //std::cout<<"Nouveau noeud alloué"<<std::endl;
      break;
    case HEAD:
      ret.type = INT;
      tmp = evalTerm(p[0],varpool).list.p_begin;
      if(tmp == nullptr) {std::cout<<"euh"<<std::endl;ret.integer = 0;}
      else ret.integer = tmp->val;
      removeRef(tmp); //Une référence de moins
      break;
    case TAIL:
      ret.type = LIST;
      tmp = evalTerm(p[0],varpool).list.p_begin;
      if(tmp == nullptr) {std::cout<<"euh"<<std::endl;ret.list = {nullptr};}
      else {
        ret.list = {tmp->p_next};
        addRef(tmp->p_next);
        removeRef(tmp);
      }
      break;
    case ISNIL:
      ret.type = BOOL;
      tmp = evalTerm(p[0],varpool).list.p_begin;
      ret.boolean = (tmp == nullptr);
      removeRef(tmp);
      break;
    case IF:
      if(evalTerm(p[0],varpool).boolean) {
        ret = evalTerm(p[1],varpool);
      }else{
        ret = evalTerm(p[2],varpool);
      };
      break;
    case PRINT:
      ret = evalTerm(p[0],varpool);
      printVal(ret, std::cout);
  }
  return ret;
}

Val evalFunctionCall(Function* function, Term* params, Val* varpool) {
  Val* newpool = new Val[function->arity.size];
  for (size_t i = 0; i < function->arity.size; i++) {
    newpool[i] = evalTerm(params[i], varpool);
  }
  Val ret = evalTerm(*function->p_term, newpool);
  for (size_t i = 0; i < function->arity.size; i++) {
    if(newpool[i].type == LIST) removeRef(newpool[i].list.p_begin);
  }
  delete[] newpool;
  return ret;
}
