#include "term.hpp"

Arity arity(BuiltinOp op) {
  static Type Int[1]{INT};
  static Type Bool[1]{BOOL};
  static Type List[1]{LIST};
  static Type Int_Int[2]{INT, INT};
  static Type Bool_Bool[2]{BOOL, BOOL};
  static Type Int_List[2]{INT, LIST};

  switch (op) {
    case NEG:
      return {Int,1};
    case NOT:
      return {Bool,1};
    case ADD:
    case SUB:
    case MUL:
    case DIV:
    case MOD:
    case EQ:
    case NEQ:
    case SUP:
    case SUPEQ:
    case INF:
    case INFEQ:
      return {Int_Int,2};
    case AND:
    case OR:
      return {Bool_Bool,2};
    case CONS:
      return {Int_List,2};
    case HEAD:
    case TAIL:
    case ISNIL:
      return {List,1};
    case IF: //Variadic arity has to be hardcoded
      return {nullptr,3};
    case PRINT:
      return {nullptr,1};
  }
  return {nullptr,0};
}

void initValueTerm(Term* p_term, Val val) {
  p_term->tag = Term::VALUE;
  p_term->value = val;
  p_term->type = val.type;
}

void initVariableTerm(Term* p_term, size_t id, Type type) {
  p_term->tag = Term::VARIABLE;
  p_term->variable.id = id;
  p_term->type = type;
}

void initOperationTerm(Term* p_term, BuiltinOp op, Term* parameters) {
  p_term->tag = Term::OPERATION;
  p_term->operation.op = op;
  p_term->operation.parameters = parameters;
  Arity ar = arity(op);
  if(ar.types != nullptr) {
    for (size_t i = 0; i < ar.size; i++) {
      if(parameters[i].type != ar.types[i])
        throw MinilispError{MinilispError::TYPE_ERROR,"Type du "+std::to_string(i+1)+"e paramètre incorrect"};
    }
  } else if(op == IF) {
    if(parameters[0].type != BOOL) throw MinilispError{MinilispError::TYPE_ERROR,"Type du 1e paramètre incorrect"};
    if(parameters[1].type != parameters[2].type) throw MinilispError{MinilispError::TYPE_ERROR,"Type des 2e et 3e paramètres différents"};
  }
  switch (op) {
    case NEG:
    case ADD:
    case SUB:
    case MUL:
    case DIV:
    case MOD:
    case HEAD:
      p_term->type = INT;
      break;
    case NOT:
    case EQ:
    case NEQ:
    case SUP:
    case SUPEQ:
    case INF:
    case INFEQ:
    case AND:
    case OR:
    case ISNIL:
      p_term->type = BOOL;
      break;
    case CONS:
    case TAIL:
      p_term->type = LIST;
      break;
    case IF:
    case PRINT:
      p_term->type = parameters[1].type;
      break;
  }
}

void initCallTerm(Term* p_term, Function* function, Term* parameters) {
  p_term->type = INT; //Par défaut
  p_term->tag = Term::CALL;
  p_term->call.function = function;
  p_term->call.parameters = parameters;
  if (function == nullptr) return; //La suite nécessite de déréférencer
  p_term->type = (function == nullptr) ? INT : function->rettype;
  for (size_t i = 0; i < function->arity.size; i++) {
    if(parameters[i].type != function->arity.types[i])
      throw MinilispError{MinilispError::TYPE_ERROR,"Type du "+std::to_string(i+1)+"e paramètre incorrect"};
  }
}

void destroyTerm(Term* ptr) {
  size_t n;
  switch (ptr->tag) {
    case Term::VALUE:
    case Term::VARIABLE:
      break;
    case Term::OPERATION:
      n = arity(ptr->operation.op).size;
      for (size_t i = 0; i < n; i++) {
        destroyTerm(ptr->operation.parameters+i);
      }
      delete[] ptr->operation.parameters;
      break;
    case Term::CALL:
      n = ptr->call.function->arity.size;
      for (size_t i = 0; i < n; i++) {
        destroyTerm(ptr->call.parameters+i);
      }
      delete[] ptr->call.parameters;
      break;
  }
}
