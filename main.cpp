#include <iostream>
#include <fstream>
#include <sstream>

#include "core/evaluation.hpp"
#include "core/parser.hpp"
#include "core/interactive.hpp"

int main(int argc, char const *argv[]) {
  std::cout<<"Tutturuu !"<<std::endl;
  /*
  MinilispType types[1]{INT};
  MinilispFunction fac{{},{types,1}};
  MinilispFunction even{{},{types,1}};
  MinilispFunction syracuse_next{{},{types,1}};
  MinilispFunction syracuse_count{{},{types,1}};
  MinilispFunction main{{},{types,1}};

  Term terms[11]{};

  fac.term = terms;
  terms[0] = operationTerm(IF,terms+1);
  terms[1] = operationTerm(EQ,terms+4);
  terms[2] = valueTerm(minilispInt(1));
  terms[3] = operationTerm(MUL,terms+6);
  terms[4] = parameterTerm(0ul, INT);
  terms[5] = valueTerm(minilispInt(0));
  terms[6] = parameterTerm(0ul, INT);
  terms[7] = callTerm(&fac,terms+8);
  terms[8] = operationTerm(SUB, terms+9);
  terms[9] = parameterTerm(0ul, INT);
  terms[10] = valueTerm(minilispInt(1));

  Term params[1]{};
  params[0].tag = Term::VALUE;
  params[0].type = INT;
  params[0].value.type = INT;
  params[0].value.integer = 20;

  std::cout<<evalFunctionCall(&fac, params).integer<<std::endl;

  MinilispParameterList* p = new MinilispParameterList{"n",INT,nullptr};
  std::istringstream ss1("(= (% n 2) 0)");
  even.term = new Term(parseTerm(ss1,nullptr,p));

  std::cout<<evalFunctionCall(&even, params).boolean<<std::endl;

  ss1 = std::istringstream("(if (= (% n 2) 0) (/ n 2) (+ (* 3 n) 1))");
  syracuse_next.term = new Term(parseTerm(ss1,nullptr,p));
  delete p;

  std::cout<<evalFunctionCall(&syracuse_next, params).integer<<std::endl;

  MinilispFunctionList functionPool{"even",even,nullptr};

  //=====================================//

  std::string s = "(if (even 0) (+ 1 1) (/ 6 (~ 2)))";
  std::istringstream ss(s);
  Term t{};
  try{
    t = parseTerm(ss,&functionPool,nullptr);
    std::cout<<evalTerm(t).integer<<std::endl;
    destroyTerm(&t);
  }catch(InterpretationError){
    std::cout<<"Il y a une erreur dans votre programme :D"<<std::endl;
  }catch(...){
    std::cerr<<"Erreur fatale ! Cause probable : manque de mémoire"<<std::endl;
  }

  std::cout<<"============================"<<std::endl;

  MinilispFunctionList* pool = nullptr;
  std::ifstream file;

  file.open("data/even.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/fac.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/syracuse_next.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/syracuse_count.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/pow.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/abs.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/gcd.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/fib.mlisp");
  pool = load(pool, file);
  file.close();

  file.open("data/main.mlisp");
  pool = load(pool, file);
  file.close();

  evalFunctionCall(&(pool->head), nullptr);
  clear(pool);
  std::cout<<fib(35)<<std::endl;*/

  std::string folder;
  if(argc > 1) folder = argv[1];
  InteractiveSession session{{nullptr},folder};
  std::string instruction;
  do {
    std::cout << ">>> ";
    std::getline(std::cin,instruction);
  } while(execute(&session,instruction,std::cout));
  destroyInteractiveSession(&session);
}
